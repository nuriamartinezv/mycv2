import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";

const initialState = {
  test: '',
};

const reducer = (state = initialState, action) => {
  if (action.type === "GET_NAME") {
    return { ...state, test: action.payload }
  }
  return state;
}

const makeStore = (init) => {
  return createStore(reducer, init, applyMiddleware(thunk));
}

export default makeStore;