import { Provider } from 'react-redux';
import App from 'next/app';
import withRedux from 'next-redux-wrapper';
import makeStore from '../store';

class MyApp extends App {
  render() {
    const { Component, store } = this.props;
    return (
      <Provider store={store}>
        <Component />
      </Provider>
    );
  }
}

export default withRedux(makeStore)(MyApp);