import React, { Component } from 'react';

import fetch from 'isomorphic-unfetch';
import { connect } from 'react-redux';

const Index = props => (
  <div>
    <p>{props.test}</p>
    <p>{props.test}</p>
  </div>
)

Index.getInitialProps = async ({store}) => {
  console.log('up');
  const url = 'https://graphql.datocms.com/';
  const opt = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': `5123061760e574334df9877311749a`,
    },
    body: JSON.stringify({
      query: `{ 
        personal {
          name
        }
      }`
    }),
  };
  await fetch(url, opt)
    .then(res => res.json())
    .then(res => {
      const input = res.data.personal.name;
      store.dispatch({type: "GET_NAME", payload: input});
    })
  return {}
};

  const mapStateToProps = state => {
    return {
      test: state.test
    };
  };

  export default connect(
    mapStateToProps,
  )(Index);